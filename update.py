import ConfigParser
import logging
import os
import sys

import requests


def exit_error(error_message):
    logging.error(error_message)
    sys.exit(1)


def main():
    login_url = "https://uflix.com.au/u/login/login"
    update_url = "https://uflix.com.au/u/my/auth-ip"
    username, password = "", ""

    file_path = os.path.join(os.path.dirname(os.path.realpath(__file__)))

    # Setup logging:
    logfile = os.path.join(file_path, 'update.log')
    logging.basicConfig(filename=logfile, level=logging.DEBUG,
                        format='%(asctime)s %(message)s',
                        datefmt='%m/%d/%Y %I:%M:%S %p')

    # Read the config file:
    try:
        config_file = ConfigParser.ConfigParser()
        config_file.read(os.path.join(file_path, 'config.cfg'))
        username = config_file.get('credentials', 'username')
        password = config_file.get('credentials', 'password')
    except Exception as e:
        exit_error("Unable to read config file due to exception: {}.".format(e))

    # Login and update, using session context. Token is handled by requests magic.
    try:
        with requests.Session() as s:
            payload = {'login': username, "password": password}
            r = s.post(login_url, data=payload)

            if r.status_code == 200:
                r = s.post(update_url)
                if r.status_code == 200:
                    logging.info("Successfully updated.")
                elif r.status_code == 403:
                    exit_error("Wrong username and/or password.")
                else:
                    exit_error("Unable to update due to HTTP Error: {}.".format(r.status_code))
            else:
                exit_error("Unable to login due to HTTP Error: {}.".format(r.status_code))
    except Exception as e:
        exit_error("Unable to proceed due to exception: {}.".format(e))

if __name__ == "__main__":
    main()
