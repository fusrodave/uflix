# README #

### What is this repository for? ###

* https://uflix.com.au matches accounts to IP.
* IP can change depending on your ISP.
* This is updated by submitting a change to uflix through the account panel by clicking a button.
* This repo scripts this automagically by logging in user a config file and updating.

### How do I get set up? ###

* Copy sample_config.cfg to config.cfg.
* Update username / password.
* Add script to cron / other scheduler.